<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Read utilizada para listar os registros
 *
 * @author Marcelo
 */
class Read extends Conn {

    //put your code here
    private $Select;
    private $Places;
    private $Result;

    /**  @var PDOStantement  */
    private $Read;

    /**  @var PDO  */
    private $Conn;

    /**
     * 
     * @param string $Table =  Informe o nome da tabela do banco
     * @param array $Dados =  Array associativo (Nome_coluna => valor)
     */
    
    public function ExeRead($Table, $Termos = null, $ParseString = null) {
        if (!empty($ParseString)):
            parse_str($ParseString, $this->Places);
        endif;
        $this->Select = "SELECT * FROM {$Table} {$Termos}";
        $this->Execute();
    }

    public function getResult() {
        return $this->Result;
    }

    public function getRownCount() {
        return $this->Read->rowCount();
    }

    /**
     * Utilizado para query manual
     */
    public function FullQuery($Query, $ParseString = null) {
        $this->Select = (string) $Query;
        if (!empty($ParseString)):
            parse_str($ParseString, $this->Places);
        endif;
        $this->Select = "SELECT * FROM {$Table} {$Temos}";
        $this->Execute();
    }

    public function setPlaces($ParseString) {
        parse_str($ParseString, $this->Places);
        $this->Execute();
    }

    /**
     * **************************************************
     * ****************** PRIVATE METHODS ****************
     * **************************************************
     */

    /** Conectar com a PDO */
    private function Connect() {
        $this->Conn = parent::getConn();
        $this->Read = $this->Conn->prepare($this->Select);
        $this->Read->setFetchMode(PDO::FETCH_ASSOC);
    }

    /** monta a sytax */
    private function getSytax() {
        if ($this->Places):
            foreach ($this->Places as $key => $value):
                if ($key == 'limit' || $key == 'offset'):
                    $value = (int) $value;
                endif;
                $this->Read->bindValue(":{$key}", $value, (is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR));
            endforeach;
        endif;
    }

    /** Executa */
    private function Execute() {
        $this->Connect();
        try {
            $this->getSytax();
            $this->Read->execute();
            $this->Result = $this->Read->fetchAll();
        } catch (PDOException $e) {
            $this->Result = null;
            WSErro("<b>Erro ao Selecionar Registro:</b> {$e->getMessage()}", $e->getCode());
        }
    }

}
