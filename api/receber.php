<?php
/**
 * Created by PhpStorm.
 * User: Marcelo
 * Date: 09/04/2018
 * Time: 17:15
 */

include_once "../database/Conn.class.php";
include_once "../database/Read.class.php";
include_once "../database/Create.class.php";

if(!isset($_REQUEST['key']) or ($_REQUEST['key'] != "labiras") ){
    echo json_encode(["status"=> "error", "msg"=>"Permissão negada"]);
    die;
}

if(!isset($_REQUEST['dados']) or empty($_REQUEST['dados'])){
    echo json_encode(["status"=> "error", "msg"=>"Dados inválidos"]);
    die;
}

$stringArray = explode('t',$_REQUEST['dados']);

if(sizeof($stringArray) != 2 ){
    echo json_encode(["status"=> "error", "msg"=>"Dados inválidos tamanho de entrata inferior"]);
    die;
}

$temperatura = $stringArray[0];
$umidade = $stringArray[1];

if(!isset($temperatura) or empty($temperatura)){
    echo json_encode(["status"=> "error", "msg"=>"Temperatura Inválida"]);
    die;
}

if(!isset($umidade) or empty($umidade)){
    echo json_encode(["status"=> "error", "msg"=>"Umidade Inválida"]);
    die;
}

$dados  = array("umidade" => trim($umidade),"temperatura" => trim($temperatura));
$reg = new Create();
unset($dados['key']);
$reg->ExeCreate('dados',$dados);
if ($reg->getResult()){
    echo json_encode(["status"=> "success", "msg"=>"Dados registrados"]);
}
function WSErro($string){
    echo json_encode(["status"=> "error", "msg"=> $string]);
}






