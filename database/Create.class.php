<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Create
 *
 * @author Marcelo
 */
class Create extends Conn {

    //put your code here
    private $Tabela;
    private $Dados;
    private $Result;

    /**  @var PDOStantement  */
    private $Create;

    /**  @var PDO  */
    private $Conn;

    /**
     * 
     * @param string $Table =  Informe o nome da tabela do banco
     * @param array $Dados =  Array associativo (Nome_coluna => valor)
     */
    public function ExeCreate($Table, array $Dados) {
        $this->Tabela = (string) $Table;
        $this->Dados = $Dados;
        $this->getSytax();
        $this->Execute();
    }

    public function getResult() {
        return $this->Result;
    }

    /**
     * **************************************************
     * ****************** PRIVATE METHODS ****************
     * **************************************************
     */

    /** Conectar com a PDO */
    private function Connect() {
        $this->Conn = parent::getConn();
        $this->Create = $this->Conn->prepare($this->Create);
    }

    /** monta a sytax */
    private function getSytax() {
        $Fields = implode(', ', array_keys($this->Dados));
        $Places = ':' . implode(', :', array_keys($this->Dados));
        $this->Create = "INSERT INTO {$this->Tabela} ({$Fields}) VALUES ({$Places})";
    }

    /** Executa */
    private function Execute() {
        $this->Connect();
        try {
            $this->Create->execute($this->Dados);
            $this->Result = $this->Conn->lastInsertId();
        } catch (PDOException $e) {
            $this->Result = null;
            WSErro("<b>Erro ao cadastrar:</b> {$e->getMessage()}", $e->getCode());
        }
    }

}
