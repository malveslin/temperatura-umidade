<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Classe abstrata  de conexao. Padrão SingleTon
 * Retorna o um objeto pdo pelo getCon()
 *
 * @author Marcelo Alves
 * @copyright (c) 2017, Marcelo Alves
 */
class Conn {

    private static $Host = "localhost";
    private static $User = "root";
    private static $Pass = "";
    private static $Dbsa = "dht11";
    private static $Type = "mysql";

    /** @var PDO */
    private static $Connect = null;

    private static function Conectar() {
        try {
            /**
             *  se a não existe conexao SINGLETON
             */
            if (self::$Connect == null):
                switch (self::$Type):
                    case 'mysql':
                        $dsn = 'mysql:host=' . self::$Host . ';dbname=' . self::$Dbsa;
                        break;
                    default :
                        $dsn = 'mysql:host=' . self::$Host . ';dbname=' . self::$Dbsa;

                endswitch;

                $options = [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8'];
                self::$Connect = new PDO($dsn, self::$User, self::$Pass, $options);

            endif;
        } catch (PDOException $e) {
            PHPErro($e->getCode(), $e->getMessage(), $e->getFile(), $e->getFile());
            die;
        }

        self::$Connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$Connect;
    }

    /**
     * 
     * @return PDO SingleTon  Pattern
     */
    public static function getConn() {
        return self::Conectar();
    }

}
