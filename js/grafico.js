$(document).ready(function(){
  $.ajax({
    url : "data.php",
    type : "GET",
    success : function(data){

      var array_temperatura = [];
      var array_umidade = [];
      var array_horario = [];

      for(var i in data) {
        array_temperatura.push(data[i].temperatura);
        array_umidade.push(data[i].umidade);
        array_horario.push(data[i].horario);
      }

      var chartdata = {
        labels: array_horario,
        datasets: [
          {
            label: "Temperatura",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(59, 89, 152, 0.75)",
            borderColor: "rgba(59, 89, 152, 1)",
            pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
            pointHoverBorderColor: "rgba(59, 89, 152, 1)",
            data: array_temperatura
          },
          {
            label: "Umidade",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(29, 202, 255, 0.75)",
            borderColor: "rgba(29, 202, 255, 1)",
            pointHoverBackgroundColor: "rgba(29, 202, 255, 1)",
            pointHoverBorderColor: "rgba(29, 202, 255, 1)",
            data: array_umidade
          }
        ]
      };

      var ctx = $("#Graph");

      var LineGraph = new Chart(ctx, {
        type: 'line',
        data: chartdata
      });
    },
    error : function(data) {

    }
  });
});