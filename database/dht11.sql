-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 17-Abr-2018 às 16:35
-- Versão do servidor: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dht11`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dados`
--

CREATE TABLE `dados` (
  `id` int(11) NOT NULL,
  `temperatura` varchar(4) NOT NULL,
  `umidade` varchar(4) NOT NULL,
  `horario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `chegou` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dados`
--

INSERT INTO `dados` (`id`, `temperatura`, `umidade`, `horario`, `chegou`) VALUES
(1, '25', '40', '2018-04-17 19:24:22', NULL),
(2, '24', '40', '2018-04-17 19:24:27', NULL),
(3, '24', '40', '2018-04-17 19:24:32', NULL),
(4, '24', '40', '2018-04-17 19:24:37', NULL),
(5, '24', '40', '2018-04-17 19:24:42', NULL),
(6, '24', '40', '2018-04-17 19:24:47', NULL),
(7, '24', '40', '2018-04-17 19:24:53', NULL),
(8, '24', '40', '2018-04-17 19:24:58', NULL),
(9, '24', '40', '2018-04-17 19:25:03', NULL),
(10, '24', '40', '2018-04-17 19:25:08', NULL),
(11, '24', '40', '2018-04-17 19:25:13', NULL),
(12, '24', '40', '2018-04-17 19:25:18', NULL),
(13, '225', '40', '2018-04-17 19:27:13', NULL),
(14, '25', '40', '2018-04-17 19:27:18', NULL),
(15, '25', '40', '2018-04-17 19:27:23', NULL),
(16, '25', '40', '2018-04-17 19:28:49', NULL),
(17, '25', '40', '2018-04-17 19:28:54', NULL),
(18, '25', '40', '2018-04-17 19:28:59', NULL),
(19, '25', '38', '2018-04-17 19:34:15', NULL),
(20, '25', '38', '2018-04-17 19:34:20', NULL),
(21, '25', '38', '2018-04-17 19:34:25', NULL),
(22, '25', '38', '2018-04-17 19:34:30', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dados`
--
ALTER TABLE `dados`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dados`
--
ALTER TABLE `dados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
